FROM registry.gitlab.com/mlreef/mlreef:latest 

LABEL maintainer="mlreef.com"

ARG CI_COMMIT_REF_SLUG
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.ci-commit-ref-slug=$CI_COMMIT_REF_SLUG \
      org.label-schema.commit-url="https://gitlab.com/mlreef/devops/nautilus-offline" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

ADD /mlreef-images-tar/ /mlreef-images-tar
